#imports the Pandas and Numpy libraries and gives aliases pd/np.
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier 
import numpy as np

# Normalisation function
def normalise(data):
    return ((data - data.mean()) / data.std())

# Read in csv files
classA = pd.read_csv('../classA.csv',header=0)
probeA = pd.read_csv('../probeA.csv',header=0)
probeB = pd.read_csv('../probeB.csv',header=0)

# Delete TNA from probeA
del probeA['TNA']

# Normalise data
probeA = normalise(probeA)
probeB = normalise(probeB)

# N Fold Cross Validation - Generate Data / Test sets

regr = KNeighborsClassifier(n_neighbors=21)
regr.fit(probeA.loc[0:1000, ['c1','c2','c3','m1','m2','m3','n1','n2','n3','p1','p2','p3']], classA.loc[0:1000, 'class'])
probabilities = regr.predict_proba(probeB.loc[0:1000, ['c1','c2','c3','m1','m2','m3','n1','n2','n3','p1','p2','p3']])

np.savetxt("classB.csv", probabilities[:,1], comments='', header='"class"', fmt='%.4f', delimiter=",")
