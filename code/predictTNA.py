#imports the Pandas and Numpy libraries and gives aliases pd/np.
import pandas as pd
from sklearn import linear_model
from sklearn.model_selection import KFold
import numpy as np

# Normalisation function
def normalise(data):
    return ((data - data.mean()) / data.std())

# Read in csv files
classA = pd.read_csv('../classA.csv',header=0)
probeA = pd.read_csv('../probeA.csv',header=0)
probeB = pd.read_csv('../probeB.csv',header=0)

# Normalise data
probeA = normalise(probeA)
probeB = normalise(probeB)

regr = linear_model.Lasso(alpha=0.1)
regr.fit(probeA.loc[0:1000, ['c1','c2','c3','m1','m2','m3','n1','n2','n3','p1','p2','p3']], probeA.loc[0:1000, 'TNA'])
predictions = regr.predict(probeB.loc[0:1000, ['c1','c2','c3','m1','m2','m3','n1','n2','n3','p1','p2','p3']])

np.savetxt("tnaB.csv", predictions, comments='', header='"TNA"', fmt='%.4f', delimiter=",")
