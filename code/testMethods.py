#imports the Pandas and Numpy libraries and gives aliases pd/np.
import matplotlib.pyplot as plt
import pandas as pd
from sklearn import linear_model
from sklearn.neighbors import KNeighborsClassifier 
from sklearn.model_selection import KFold
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.metrics import explained_variance_score
import numpy as np
from random import randint

# Normalisation function
def normalise(data):
    return ((data - data.mean()) / data.std())

# Read in csv files
classA = pd.read_csv('../classA.csv',header=0)
probeA = pd.read_csv('../probeA.csv',header=0)
probeB = pd.read_csv('../probeB.csv',header=0)

# Delete TNA from probeA
del probeA['TNA']

# Normalise data
probeA = normalise(probeA)
probeB = normalise(probeB)

# N Fold Cross Validation - Generate Data / Test sets

probeA['class'] = classA['class']


kf = KFold(n_splits=5, shuffle=True)

for i in range(3,25):
    cScore = 0
    mScore = 0
    nScore = 0
    pScore = 0
    for train_index, test_index in kf.split(probeA):
        for j in range(100):
            regr = KNeighborsClassifier(n_neighbors=i)
            regr.fit(probeA.loc[train_index, ['c1','c2','c3']], probeA.loc[train_index, 'class'])
            cScore += regr.score(probeA.loc[test_index, ['c1','c2','c3']], probeA.loc[test_index, 'class'])

            regr.fit(probeA.loc[train_index, ['m1','m2','m3']], probeA.loc[train_index, 'class'])
            mScore += regr.score(probeA.loc[test_index, ['m1','m2','m3']], probeA.loc[test_index, 'class'])

            regr.fit(probeA.loc[train_index, ['n1','n2','n3']], probeA.loc[train_index, 'class'])
            nScore += regr.score(probeA.loc[test_index, ['n1','n2','n3']], probeA.loc[test_index, 'class'])

            regr.fit(probeA.loc[train_index, ['p1','p2','p3']], probeA.loc[train_index, 'class'])
            pScore += regr.score(probeA.loc[test_index, ['p1','p2','p3']], probeA.loc[test_index, 'class'])
    print 'Neighbour Value: %d, cScore: %.3f' % (i, cScore/500)
    print 'Neighbour Value: %d, mScore: %.3f' % (i, mScore/500)
    print 'Neighbour Value: %d, nScore: %.3f' % (i, nScore/500)
    print 'Neighbour Value: %d, pScore: %.3f' % (i, pScore/500)

'''
cScore = 0
mScore = 0
nScore = 0
pScore = 0
for train_index, test_index in kf.split(probeA):
    for j in range(300):
        clf = DecisionTreeClassifier(random_state=0)
        clf.fit(probeA.loc[train_index, ['c1','c2','c3']], probeA.loc[train_index, 'class'])
        cScore += clf.score(probeA.loc[test_index, ['c1','c2','c3']], probeA.loc[test_index, 'class'])

        clf.fit(probeA.loc[train_index, ['m1','m2','m3']], probeA.loc[train_index, 'class'])
        mScore += clf.score(probeA.loc[test_index, ['m1','m2','m3']], probeA.loc[test_index, 'class'])

        clf.fit(probeA.loc[train_index, ['n1','n2','n3']], probeA.loc[train_index, 'class'])
        nScore += clf.score(probeA.loc[test_index, ['n1','n2','n3']], probeA.loc[test_index, 'class'])

        clf.fit(probeA.loc[train_index, ['p1','p2','p3']], probeA.loc[train_index, 'class'])
        pScore += clf.score(probeA.loc[test_index, ['p1','p2','p3']], probeA.loc[test_index, 'class'])

print 'DecisionTreeClassifier cScore: %.3f' % (cScore/1500)
print 'DecisionTreeClassifier mScore: %.3f' % (mScore/1500)
print 'DecisionTreeClassifier nScore: %.3f' % (nScore/1500)
print 'DecisionTreeClassifier pScore: %.3f' % (pScore/1500)

cScore = 0
mScore = 0
nScore = 0
pScore = 0
for train_index, test_index in kf.split(probeA):
    for j in range(300):
        clf = linear_model.LinearRegression()
        clf.fit(probeA.loc[train_index, ['c1','c2','c3']], probeA.loc[train_index, 'class'])
        cScore += clf.score(probeA.loc[test_index, ['c1','c2','c3']], probeA.loc[test_index, 'class'])

        clf.fit(probeA.loc[train_index, ['m1','m2','m3']], probeA.loc[train_index, 'class'])
        mScore += clf.score(probeA.loc[test_index, ['m1','m2','m3']], probeA.loc[test_index, 'class'])

        clf.fit(probeA.loc[train_index, ['n1','n2','n3']], probeA.loc[train_index, 'class'])
        nScore += clf.score(probeA.loc[test_index, ['n1','n2','n3']], probeA.loc[test_index, 'class'])

        clf.fit(probeA.loc[train_index, ['p1','p2','p3']], probeA.loc[train_index, 'class'])
        pScore += clf.score(probeA.loc[test_index, ['p1','p2','p3']], probeA.loc[test_index, 'class'])

print 'Linear Regression cScore: %.3f' % (cScore/1500)
print 'Linear Regression mScore: %.3f' % (mScore/1500)
print 'Linear Regression nScore: %.3f' % (nScore/1500)
print 'Linear Regression pScore: %.3f' % (pScore/1500)

cScore = 0
mScore = 0
nScore = 0
pScore = 0
for train_index, test_index in kf.split(probeA):
    for j in range(300):
        clf = linear_model.Ridge(alpha=1.0)
        clf.fit(probeA.loc[train_index, ['c1','c2','c3']], probeA.loc[train_index, 'class'])
        cScore += clf.score(probeA.loc[test_index, ['c1','c2','c3']], probeA.loc[test_index, 'class'])

        clf.fit(probeA.loc[train_index, ['m1','m2','m3']], probeA.loc[train_index, 'class'])
        mScore += clf.score(probeA.loc[test_index, ['m1','m2','m3']], probeA.loc[test_index, 'class'])

        clf.fit(probeA.loc[train_index, ['n1','n2','n3']], probeA.loc[train_index, 'class'])
        nScore += clf.score(probeA.loc[test_index, ['n1','n2','n3']], probeA.loc[test_index, 'class'])

        clf.fit(probeA.loc[train_index, ['p1','p2','p3']], probeA.loc[train_index, 'class'])
        pScore += clf.score(probeA.loc[test_index, ['p1','p2','p3']], probeA.loc[test_index, 'class'])

print 'Ridge cScore: %.3f' % (cScore/1500)
print 'Ridge mScore: %.3f' % (mScore/1500)
print 'Ridge nScore: %.3f' % (nScore/1500)
print 'Ridge pScore: %.3f' % (pScore/1500)
for train_index, test_index in kf.split(probeA):
    for j in range(300):
        clf = linear_model.Lasso(alpha=0.1)
        clf.fit(probeA.loc[train_index, ['c1','c2','c3']], probeA.loc[train_index, 'class'])
        cScore += clf.score(probeA.loc[test_index, ['c1','c2','c3']], probeA.loc[test_index, 'class'])

        clf.fit(probeA.loc[train_index, ['m1','m2','m3']], probeA.loc[train_index, 'class'])
        mScore += clf.score(probeA.loc[test_index, ['m1','m2','m3']], probeA.loc[test_index, 'class'])

        clf.fit(probeA.loc[train_index, ['n1','n2','n3']], probeA.loc[train_index, 'class'])
        nScore += clf.score(probeA.loc[test_index, ['n1','n2','n3']], probeA.loc[test_index, 'class'])

        clf.fit(probeA.loc[train_index, ['p1','p2','p3']], probeA.loc[train_index, 'class'])
        pScore += clf.score(probeA.loc[test_index, ['p1','p2','p3']], probeA.loc[test_index, 'class'])

print 'Lasso cScore: %.3f' % (cScore/1500)
print 'Lasso mScore: %.3f' % (mScore/1500)
print 'Lasso nScore: %.3f' % (nScore/1500)
print 'Lasso pScore: %.3f' % (pScore/1500)
'''
